/*

	JSON or JavaScript Object Notation, is a "DATA FORMAT" used by application

		► JSON stands for JavaScript Object Notation
		► JSON is also used in other programming languages.
		► JavaScript Objects are not to be confused with JSON

	Syntax:
		{
			"propertyA": "valueA",
			"propertyB": "valueB"
		}
*/

/*// JSON Objects
{
	"city": "Quezon City",
	"province": "Metro Manila",
	"country": "Philippines"
}*/

/*// JSON Array
"cities" : [ 
			{ "city": "Quezon City",
			 "province": "Metro Manila",
			 "country": "Philippines" },

			 { "city": "Manila",
			 	"province": "Metro Manila",
			 	"country": "Philippines" },

			 { "city": "Makati City",
			 	"province": "Metro Manila",
			 	"country": "Philippines" }
]*/

// JSON Methods
/*
	The JSON object contains methods for parsing & converting data into stringified JSON

*/
// Converting Data into a springified JSON

let batchesArr = [

				{ batchName: "Batch 204" },
				{ batchName: "Batch 203" },

];
console.log(batchesArr);
console.log(JSON.stringify(batchesArr)); // TO convert object or array to JSON.stringify

let data = JSON.stringify({
	name: 'John',
	age: 18,
	address: {
		city: 'Manila',
		country: 'Philippines'
	}
})
console.log(data);


/* UNcomment this later
// USer Details
let firstName = prompt("What is your First Name?");
let lastName = prompt("What is your Last Name?");
let age = prompt("What is your Age?");
let address = {
	city: prompt("Which city do you live in?"),
	country: prompt("Which country does your country belong to?")
};

let otherData = JSON.stringify({
	firstName: firstName,
	lastName: lastName,
	age: age,
	address: address
});
console.log(otherData);
*/

// Convert stringified JSON into JS Objects using JSON.parse();
let batchesJSON = `[{"batchname": "Batch 204"}, {"batchname": "Batch 203"}]`;
console.log(batchesJSON);
console.log(JSON.parse(batchesJSON)); // to convert JSON.Stringify to original Object or Array


let stringifiedObject = `{
	"name": "john",
	"age": 18,
	"address": {
		"city": "Manila",
		"country": "Philippines"
	}
}`

console.log(JSON.parse(stringifiedObject));






